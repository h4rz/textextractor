
# TextExtractor
Extracting Text from images with the help of ML Kit.

## Setting up the project
1. You would need to add the following lines to your Podfile
```Podfile
pod 'Firebase/Core'
pod 'Firebase/MLVision'
pod 'Firebase/MLVisionTextModel'
```
2. Open the Terminal app, switch over to the project folder and run the following command to install the CocoaPods used in the project though:
```bash
$ pod install
```
3. Once the CocoaPods are installed, open _TextExtractor.xcworkspace_ in Xcode
 
4. Setting up the firebase account 
    * To set up a Firebase account, follow the account setup section in this [Getting Started With Firebase Tutorial](https://www.raywenderlich.com/187417/firebase-tutorial-getting-started-3). While the Firebase products are different, the account creation and setup is exactly the same.
    * The general idea is that you:
        * Create an account.
        * Create a project.
        * Add an iOS app to a project.
        * Drag the  _GoogleService-Info.plist_  to your project.
        * Initialize Firebase in the AppDelegate.

## Post Implementation

![Initial Screen](screenshots/1.png)
![Selected Image](screenshots/2.png)
![Extracted Text](screenshots/3.png)


## Reference -
1. Code for Image picker
    * https://medium.com/better-programming/implement-imagepicker-using-swiftui-7f2a28caaf9c
2. Text Recognition and extraction
    * https://www.raywenderlich.com/6565-ml-kit-tutorial-for-ios-recognizing-text-in-images#toc-anchor-002
    * https://firebase.google.com/docs/ml-kit/ios/recognize-text
