//
//  ContentView.swift
//  TextExtractor
//
//  Created by Harsh Rajgor on 17/03/20.
//  Copyright © 2020 Harsh Rajgor. All rights reserved.
//

import SwiftUI
import Firebase

struct ContentView: View {
    
    @State private var showImagePicker : Bool = false
    @State private var image : Image? = nil
    @State private var extractedText: Text = Text("")
    @State var uiImage : UIImage? = nil
    @State var uiImageView : UIImageView? = nil
    let processor = ScaledElementProcessor()
    
    var body: some View {
        ScrollView{
            VStack{
                image?.resizable().scaledToFit().padding()
                Spacer()
                extractedText
                Spacer()
                HStack{
                    Group{
                        Button(action: {
                            self.showImagePicker = true
                        }){
                            Text("Pick From Gallery")
                        }
                    }
                }
            }.sheet(isPresented: self.$showImagePicker){
                PhotoCaptureView(showImagePicker: self.$showImagePicker, image: self.$image, uiImage: self.$uiImage, extractedText: self.$extractedText)
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}



