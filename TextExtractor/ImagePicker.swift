//
//  ImagePicker.swift
//  TextExtractor
//
//  Created by Harsh Rajgor on 17/03/20.
//  Copyright © 2020 Harsh Rajgor. All rights reserved.
//

import SwiftUI

struct ImagePicker: UIViewControllerRepresentable {
    @Binding var isShown : Bool
    @Binding var image : Image?
    @Binding var uiImage : UIImage?
    @Binding var extractedText: Text
    
    func updateUIViewController(_ uiViewController: UIImagePickerController, context: UIViewControllerRepresentableContext<ImagePicker>) {
        
    }
    
    func makeCoordinator() -> ImagePickerCordinator {
        return ImagePickerCordinator(isShown: $isShown, image: $image, uiImage: $uiImage, extractedText: $extractedText)
    }
    
    func makeUIViewController(context: UIViewControllerRepresentableContext<ImagePicker>) -> UIImagePickerController {
        let picker = UIImagePickerController()
        picker.delegate = context.coordinator
        return picker
    }
    
}

class ImagePickerCordinator : NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    @Binding var isShown : Bool
    @Binding var image : Image?
    @Binding var uiImage : UIImage?
     @Binding var extractedText: Text
    init(isShown : Binding<Bool>, image: Binding<Image?>, uiImage: Binding<UIImage?>,extractedText: Binding<Text>) {
        _isShown = isShown
        _image = image
        _uiImage = uiImage
        _extractedText = extractedText
    }
    //Selected Image
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        uiImage = info[UIImagePickerController.InfoKey.originalImage]   as? UIImage
        uiImage = uiImage?.fixOrientation()
        image = Image(uiImage: uiImage!)
        // extract text from selected image and display
        let processor = ScaledElementProcessor()
        processor.process(in: uiImage) { text in
            self.extractedText = Text(text)
        }
        isShown = false
    }
    //Image selection got cancel
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        isShown = false
    }
}
