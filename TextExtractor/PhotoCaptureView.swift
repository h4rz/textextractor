//
//  PhotoCaptureView.swift
//  TextExtractor
//
//  Created by Harsh Rajgor on 17/03/20.
//  Copyright © 2020 Harsh Rajgor. All rights reserved.
//

import SwiftUI

struct PhotoCaptureView: View {
    
    @Binding var showImagePicker : Bool
    @Binding var image : Image?
    @Binding var uiImage : UIImage?
    @Binding var extractedText: Text
    
    var body: some View {
        ImagePicker(isShown: $showImagePicker, image: $image, uiImage: $uiImage,extractedText: $extractedText)
    }
}

struct PhotoCaptureView_Previews: PreviewProvider {
    static var previews: some View {
        PhotoCaptureView(showImagePicker: .constant(false), image:  .constant(Image("sample")), uiImage: .constant(UIImage(named: "sample")), extractedText: .constant(Text("")))
    }
}
