//
//  ScaledElementProcessor.swift
//  TextExtractor
//
//  Created by Harsh Rajgor on 17/03/20.
//  Copyright © 2020 Harsh Rajgor. All rights reserved.
//

import Firebase

class ScaledElementProcessor {
    
    let vision = Vision.vision()
    var textRecognizer: VisionTextRecognizer!
    
    init() {
        textRecognizer = vision.onDeviceTextRecognizer()
    }
    
    func process(in imageView: UIImage?,
      callback: @escaping (_ text: String) -> Void) {
      // 1
      guard let image = imageView else { return }
      // 2
      let visionImage = VisionImage(image: image)
      // 3
      textRecognizer.process(visionImage) { result, error in
        // 4
        guard
          error == nil,
          let result = result,
          !result.text.isEmpty
          else {
            callback("")
            return
        }
        // 5
        callback(result.text)
      }
    }
    
}
